#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Setting MAYANode version"
kubectl exec -it -n "$NAME" -c mayanode deploy/mayanode -- /kube-scripts/retry.sh /kube-scripts/set-version.sh
sleep 5
echo MAYANode version updated

display_status
