#!/bin/bash

RPC_PORT=${RPC_PORT:-26657}
GRPC_PORT=${GRPC_PORT:-9090}

# initialize config
if [[ ! -f "/root/.gaia/config/app.toml" ]]; then
  cp /etc/gaia/app.toml /root/.gaia/config/app.toml
fi

exec /gaiad start --log_format json --rpc.laddr "tcp://0.0.0.0:$RPC_PORT" --grpc.address "0.0.0.0:$GRPC_PORT" --x-crisis-skip-assert-invariants "$@"
